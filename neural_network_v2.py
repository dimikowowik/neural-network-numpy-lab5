import numpy as np
from matplotlib import pyplot


class NeuralNetwork:

    def __init__(self):
        np.random.seed(1)

        self.synaptic_weights = np.zeros((2, 1))
        self.b = 0
        self.dj = [0]

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def think(self, inputs):
        inputs = inputs.astype(float)
        z = np.dot(self.synaptic_weights.T, inputs) + self.b
        a = self.sigmoid(z)
        return a

    def train(self, x, y, epoch):
        J = [0]
        eps = 0.0001
        m = len(x)
        alpha = 0.1
        dw = 0
        db = 0
        for i in range(epoch):
            a = self.think(x)
            J.append(-(y * np.log(a) + (1 - y) * np.log(1 - a)))

            dz = a - y
            dw = (1 / m) * np.dot(x, dz.T)
            db = (1 / m) * np.sum(dz)
            self.synaptic_weights -= alpha * dw
            self.b -= alpha * db

            self.dj = ((1 / m) * J[i] - J[i - 1])
            if np.all(abs(self.dj) <= eps):
                break

    def plot_cost_function(self):
        for i in self.dj:
            pyplot.plot(i, 'ro--')
            pyplot.xlabel("item")
            pyplot.ylabel("J")
            pyplot.title("COST FUNCTION")
            pyplot.legend(['J = -(1/m) * (y * np.log(a) + (1 - y) * np.log(1 - a))'])
            pyplot.grid(True)
            pyplot.show()

    def plot_classifier(self, x):
        t = np.arange(-1, 2, 0.2)
        y = np.tan(40) * t + 1.5
        pyplot.plot(t, y,
                    x[0], x[1], 'go')
        pyplot.xlabel('x[0]')
        pyplot.ylabel('x[1]')
        pyplot.title('CLASSIFIER')
        pyplot.legend(['classifier',
                       'point of array x'])
        pyplot.grid(True)
        pyplot.show()


if __name__ == "__main__":

    neural_network = NeuralNetwork()

    print("Початкові ваги: ")
    print(neural_network.synaptic_weights)

    t_x = np.array([[0, 0, 1, 1],
                    [0, 1, 0, 1]])

    t_y = np.array([[0, 0, 0, 1]])

    neural_network.train(t_x, t_y, 500)

    print("Кінцеві ваги після тренування: ")
    print(neural_network.synaptic_weights)

    a = neural_network.think(t_x)
    print("Результат навчання: \n", a)
    result = []
    for i in a:
        for j in i:
            if np.all(j > 0.5):
                result.append(1)
            else:
                result.append(0)
    print("Округлені результати:\n", result)
    print('Функція втрат:\n', neural_network.dj)

    neural_network.plot_cost_function()
    neural_network.plot_classifier(t_x)

    # TESTING
    print('TESTING THE NEURAL NETWORK')
    res1 = neural_network.think(np.array([t_x[0][0], t_x[1][0]]))
    res2 = neural_network.think(np.array([t_x[0][1], t_x[1][1]]))
    res3 = neural_network.think(np.array([t_x[0][2], t_x[1][2]]))
    res4 = neural_network.think(np.array([t_x[0][3], t_x[1][3]]))

    if (res1 and res2 and res3) < 0.5:
        print('test 1, 2, 3 passed')
    else:
        print('error in test 1, 2, 3')
    if res4 >= 0.5:
        print('test 4 passed')
    else:
        print('error in test 4')
